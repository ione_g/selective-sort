
package util;

public class Sleep {
    public Sleep() {
    }

    public static void sleepFor(long nanoseconds) {
        long startTime = System.nanoTime();

        long timeElapsed;
        do {
            timeElapsed = System.nanoTime() - startTime;
        } while(timeElapsed < nanoseconds);

    }

    public static long secondsToNano(long time) {
        return time * (long)Math.pow(10.0D, 9.0D);
    }

    public static long millisecondsToNano(long time) {
        return time * (long)Math.pow(10.0D, 6.0D);
    }

    public static long microsecondsToNano(long time) {
        return time * (long)Math.pow(10.0D, 3.0D);
    }
}
