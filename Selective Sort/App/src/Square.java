

import util.Sleep;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class Square implements ImageObserver { // клас, створений для малювання квадрату
    //Поля
    private int x;
    private int y;
    private final int width;
    private int red;
    private int green;
    private int blue;
    private int number;
    private int theme; /* x,у - координати лівої верхньої точки квадрата
                                                          width - ширина квадрату і відповідо і його висота
                                                          red, green, blue - змінні які надають колір квадрату(RGB)
                                                          number - цифра, яка буде зображена на квадраті
                                                          theme - змінна, яка дозволяє малювати квадрат 2 різними темами*/
    private boolean isSorted;//якщо квадрат стоїть на своєму місті то isSorted приймає значення true
    private final BufferedImage[] drawnumber = new BufferedImage[4];//масив , який збері
    private final BufferedImage[] digit = new BufferedImage[11]; //масив, для збереження цифр від 0 до 9 і знаку"-"
    private Color framecolor;//змінна, яка відповідає за колір рамки
    //Конструктор
    public Square (int x,int y,int width,int red,int green,int blue,int number){//при ініціалізації квадрату ми задаєм основні параметри його полям
        this.x = x;
        this.y = y;
        this.width = width;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.number = number;
        this.framecolor = new Color(255, 255, 255);
        this.isSorted = false;
    }
    //Функції
    public void setDigit(BufferedImage[] digit) {
        System.arraycopy(digit, 0, this.digit, 0, digit.length);
    }

    public int getX() {
        return this.x;
    }

    public  int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public  void setRed(int red) {
        this.red = red;
    }
    public  void setFramecolor(Color framecolor) {
        this.framecolor = framecolor;
    }
    public int getRed() {
        return red;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public  int getGreen() {
        return green;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public int getBlue() {
        return blue;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
    public void uprect(long milliSecDelay){ //функція, яка піднімає квадрат вверх
        int upnumber = this.width+2;
        for(int i = 0; i < upnumber;i++) {
            this.y -= 1;
            Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay));
        }
    }

    public  void moveLeft() {   //функція рухає квадрат на 1 піксель вліво
        this.x -= 1;
    }

    public  void moveRight() {  //функція рухає квадрат на 1 піксель вправо
        this.x += 1;
    }

    public  void moveUp() {    //функція рухає квадрат на 1 піксель вверх
        this.y -= 1;
    }

    public  void moveDown() {   //функція рухає квадрат на 1 піксель вниз
        this.y += 1;
    }

    public int countOfdigits() {    //функція повертає розмір числа включно з мінусом , якщо він є
        return Integer.toString(this.number).length();
    }

    public void getImagesIndexes() {                    // функція, яка створює масив картинок (чисел), які малюються на квадраті
        String snumber = Integer.toString(this.number);
        for (int i = 0;i<snumber.length();i++) {
            switch (snumber.charAt(i)) {
                case '-' -> drawnumber[i] = digit[10];
                case '0' -> drawnumber[i] = digit[0];
                case '1' -> drawnumber[i] = digit[1];
                case '2' -> drawnumber[i] = digit[2];
                case '3' -> drawnumber[i] = digit[3];
                case '4' -> drawnumber[i] = digit[4];
                case '5' -> drawnumber[i] = digit[5];
                case '6' -> drawnumber[i] = digit[6];
                case '7' -> drawnumber[i] = digit[7];
                case '8' -> drawnumber[i] = digit[8];
                case '9' -> drawnumber[i] = digit[9];
            }
        }
    }

    public void draw(Graphics2D g) {    // функція яка малює квадрати
        getImagesIndexes();
        switch (theme) { // в залежності від теми буде вибрано спосіб зображення квадратів
            case 0 -> {//перша тема малює тільки його колір
                g.setColor(new Color(red, green, blue));
                g.fillRect(x, y, width, width);
            }
            case 1 -> { //друга малює цифри на квадратах
                g.setColor(framecolor);
                g.setStroke(new BasicStroke(2));
                g.drawRect(x + 2, y + 2, width - 4, width - 4);
                switch (countOfdigits()) {
                    case 1 ->    // зображення одноцифрового числа
                            g.drawImage(drawnumber[0], x, y + 5, width, width - 10, null);
                    case 2 -> {    // зображення 2-цифрового числа або одно-цифрового з мінусом
                        g.drawImage(drawnumber[0], x, y + 5, width / 2, width - 10, null);
                        g.drawImage(drawnumber[1], x + width / 2, y + 5, width / 2, width - 10, null);
                    }
                    case 3 -> {    // зображення 3-цифрового числа або дво-цифрового з мінусом
                        g.drawImage(drawnumber[0], x, y + 5, width / 3, width - 10, null);
                        g.drawImage(drawnumber[1], x + width / 3, y + 5, width / 3, width - 10, null);
                        g.drawImage(drawnumber[2], x + 2 * width / 3, y + 5, width / 3, width - 10, null);
                    }
                    case 4 -> {     // зображення 3-цифрового числа з мінусом
                        g.drawImage(drawnumber[0], x, y + 5, width / 4, width - 10, null);
                        g.drawImage(drawnumber[1], x + width / 4, y + 5, width / 4, width - 10, null);
                        g.drawImage(drawnumber[2], x + 2 * width / 4, y + 5, width / 4, width - 10, null);
                        g.drawImage(drawnumber[3], x + 3 * width / 4 - 2, y + 5, width / 4, width - 10, null);
                    }
                }
            }
        }
            if (isSorted) {
                g.setColor(new Color(209, 255, 0));
                g.setStroke(new BasicStroke(2));
                g.drawRect(x+1,y+1,width-2,width-2);
        }

    }

    @Override
    public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
        return false;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public void setSorted(boolean sorted) {
        isSorted = sorted;
    }
}
