
import util.Sleep;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;



public class MyPanel extends JPanel implements ActionListener { // клас MyPanel наслідує клас JPanel і інтерфейс
                                                                // ActionListener для створення власного поля для малювання
    //поля класу MyPanel
        private static BufferedImage titleimg = null;  // зображення титулки
        final int PANEL_WIDTH = 1920;   //  ширина вікна(панелі)
        final int PANEL_HEIGHT = 1080;  // висота вікна(панелі)
    public int n;  // кількість квадратів на тлі
        private final int[][] framerect = new int[2][6]; //масив який зображує прямокутники які проходяться по квадратах
        public Square[] square;    //масив квадратів
        public boolean check;   //провіряє чи проініціалізовано поля
        public Color gradient = new Color(118,232,150);  //початковий
        public Color gradient2 = new Color(140, 70, 255);   //і кінцевий кольори градієнту
        public long speed = 300;//швидкість візуалізації
        private static BufferedImage[] digit;

    MyPanel(){  //конструктор класу
        this.setPreferredSize(new Dimension(PANEL_WIDTH,PANEL_HEIGHT));
        this.setBackground(Color.darkGray);
        //створює події, які використовуються для перемалбовування панелі
        Timer timer = new Timer(1, this);
        timer.start();
        for (int i = 0; i<2;i++) {
            this.framerect[i][0] = 0;
            this.framerect[i][1] = 0;
            this.framerect[i][2] = 0;
            this.framerect[i][3] = 0;
            this.framerect[i][4] = 0;
            this.framerect[i][5] = 0;
        }
            initialDigits();
    }

    public static void initialDigits() {
        BufferedImage img = null;
        BufferedImage img2 = null;

        try {
            img = ImageIO.read(Objects.requireNonNull(MyPanel.class.getClassLoader().getResourceAsStream("numbers/numbers.png")));
            img2 = ImageIO.read(Objects.requireNonNull(MyPanel.class.getClassLoader().getResourceAsStream("numbers/minus.png")));
            titleimg = ImageIO.read(Objects.requireNonNull(MyPanel.class.getClassLoader().getResourceAsStream("numbers/Title1.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //об'єкт для обрізання зображення
        ImageCutter numimg = new ImageCutter(img);
        int col = 1;
        int row = 1;
        digit = new BufferedImage[11];
        for (int j = 1; j<=10;j++) {
            if (col == 6) {
                col = 1;
                row++;
            }
            digit[j - 1] = numimg.grabImage(col, row, 216, 295);
            col++;
        }
        digit[10] = img2;
    }

    public void swapcoloran(int firstIndex, int secondIndex, long milliSecDelay) { //функція, яка міняє місцями квадрати
                                                                                    //з анімацією
        Square temp1 = this.square[firstIndex];      //temp1 змінна для обміну силок square
        int steps = secondIndex - firstIndex;       //steps визначає скільки ще квадратів між заданими для того,
                                                // щоб знати скільки разів викликати функцію
                                                // rightandleftrect(secondIndex,firstIndex,milliSecDelay);
        square[firstIndex].uprect(milliSecDelay/50);//піднімає перший квадрат
        uprectf(secondIndex, milliSecDelay/50);//піднімає 2 квадрат
        for (int i = 0; i<steps;i++) {
            rightandleftrect(secondIndex,firstIndex,milliSecDelay);//рухає квадрати вправо і вліво
        }
        downrect(firstIndex,secondIndex,milliSecDelay);//опускає два квадрати
        this.square[firstIndex] = this.square[secondIndex]; //квадрати міняються силками
        this.square[secondIndex] = temp1;
        this.repaint();
        Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay));
    }

    public void swapcolor(int firstIndex, int secondIndex, long milliSecDelay) { //функція, яка міняє значення квадратів у масиві
        int temp1 = this.square[firstIndex].getRed();
        int temp2 = this.square[firstIndex].getGreen();
        int temp3 = this.square[firstIndex].getBlue();
        int temp4 = this.square[firstIndex].getNumber();
        this.square[firstIndex].setRed(this.square[secondIndex].getRed());
        this.square[firstIndex].setGreen(this.square[secondIndex].getGreen());
        this.square[firstIndex].setBlue(this.square[secondIndex].getBlue());
        this.square[firstIndex].setNumber(this.square[secondIndex].getNumber());
        this.square[secondIndex].setRed(temp1);
        this.square[secondIndex].setGreen(temp2);
        this.square[secondIndex].setBlue(temp3);
        this.square[secondIndex].setNumber(temp4);
        this.repaint();
        Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay));
    }


    public void uprectf(int firstIndex,  long milliSecDelay){
        int upnumber =this.square[firstIndex].getWidth()+2;

        for(int i = 0; i < upnumber;i++) {
            this.square[firstIndex].moveUp();
            this.framerect[1][1] -= 1;
            Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay));
            this.repaint();
        }
    }
    public void downrect(int firstIndex,int secondIndex, long milliSecDelay){ //функція опускає квадрати на місце
        int downnumber = this.square[firstIndex].getWidth()+2;
        for(int i = 0; i < downnumber;i++) {
            this.square[firstIndex].moveDown();
            this.square[secondIndex].moveDown();
            this.framerect[1][1] += 1;
            Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay/100));
            this.repaint();
        }
    }

    public void rightandleftrect(int firstIndex, int secondIndex, long milliSecDelay) { // функція міняє квадрати місцями
        int rectwidth = this.square[firstIndex].getWidth()+1;
        for(int i = 0; i < rectwidth;i++) {
            this.framerect[1][0] -=1;
            this.square[firstIndex].moveLeft();
            this.square[secondIndex].moveRight();
            Sleep.sleepFor(Sleep.millisecondsToNano(milliSecDelay/100));
            this.repaint();
        }
    }


    public void highLightFrame (int index, long millisecondDelay) { //функція яка зображає рамку, яка проходиться по масиву і шукає мінімальний елемент
        this.framerect[0][0] = this.square[index].getX(); //заповнюється елемент масиву, який відповідає за x координату рамки
        this.framerect[0][1] = this.square[index].getY(); //заповнюється елемент масиву, який відповідає за y координату рамки
        this.framerect[0][2] = 150;//встановлення кольору рамки
        this.framerect[0][3] = 30;
        this.framerect[0][4] = 30;
        this.framerect[0][5] = this.square[index].getWidth();//відповідає за висоту і ширину рамки
        Sleep.sleepFor(Sleep.millisecondsToNano(millisecondDelay));
        this.framerect[0 ][5] = 0; // для того щоб рамка зникла анульовуємо її висоту і ширину
    }

    public void minHighLight(int index, long millisecondDelay) { // функція, яка виділяє найменший елемент
        this.framerect[1][0] = this.square[index].getX();//заповнюється елемент масиву, який відповідає за x координату рамки
        this.framerect[1][1] = this.square[index].getY();//заповнюється елемент масиву, який відповідає за y координату рамки
        this.framerect[1][2] = 10;//встановлення кольору рамки
        this.framerect[1][3] = 200;
        this.framerect[1][4] = 10;
        this.framerect[1][5] = this.square[index].getWidth();
        Sleep.sleepFor(Sleep.millisecondsToNano(millisecondDelay/4));
    }



    public void runSort() { //Функція, яка віуалізує сортування.
        int len = this.n;
        int minimum;
        for(int i = 0; i < len-1; ++i) {// перший цикл для сортування тут усе згідно теорії сортування вибором
            int minIndex = i;//присвоюємо і мінімальному значенню
            minimum = square[minIndex].getNumber();
            minHighLight(i, speed);//виділяємо мінімальне значення зеленою рамкою
            Sleep.sleepFor(Sleep.millisecondsToNano(speed));//затримуємо repaint();
            for(int j = i + 1; j < len; ++j) {//вкладений цикл тут також все згідно теорії сортування вибором
                highLightFrame(j,speed);//виділяємо поточний квадрат червоною рамкою
                if (square[j].getNumber() < minimum) {//якщо число менше за число, квадрата з цифрою
                                                      // початку значення якого знаходиться у масиві resetcolr
                    minimum = square[j].getNumber(); //міняємо значення мінімуму
                    minHighLight(j, speed); //виділяємо його
                    minIndex = j;//встановлюємо значення minIndex на значення індекса поточного квадрата
                }
            }
            if (i!=minIndex){//якщо значення minIndex = i тоді квадрат знаходиться на своєму місці анімації не потрібно
                swapcoloran(i, minIndex, speed);//анімація, яка міняє квадрати місцями
            }
            square[i].setSorted(true);//встановлюємо шо квадрат на своєму місці, щоб він виділився
        }
        for (int i = 0; i<2;i++) {//обнулюємо значення у рамках, щоб вони зникли з екрану
            this.framerect[i][0] = 0;
            this.framerect[i][1] = 0;
            this.framerect[i][2] = 0;
            this.framerect[i][3] = 0;
            this.framerect[i][4] = 0;
            this.framerect[i][5] = 0;
        }
        for (int i = len-2; i >=0 ; --i) {
            square[i].setSorted(false);//виключаємо виділення квадрату
            Sleep.sleepFor(Sleep.millisecondsToNano(50));
        }
    }

    public void shuffleColor() {
        Random rng = new Random();
        for (int i = 0; i < this.n; ++i) {
            int swapWithIndex = rng.nextInt(this.n - 1);
            this.swapcolor(i, swapWithIndex, 20L);
        }
    }

    public void refresher() {
        int n = this.n;
        square = new Square[n];
        int[] numbers = new int[this.n];
        for (int i = 0; i<n; i++) {
            numbers[i] = (int) (Math.random()*(999+999)-999); //генеруэмо рандомні числа від -999 до 999, які будуть зображені на квадратиках
        }
        for (int i = 0; i < numbers.length; i++) { // використовуючи відомий алгоритм сортування вибором
                                                    // впорядковуємо числа отримані в попередньому кроці
            int pos = i;
            int min = numbers[i];
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] < min) {
                    pos = j;
                    min = numbers[j];
                }
            }
            numbers[pos] = numbers[i];
            numbers[i] = min;
        }
        int r = gradient2.getRed();
        int g = gradient2.getGreen();
        int b = gradient2.getBlue();
        int dr = (gradient.getRed() - gradient2.getRed() )/n; //визначення різниці червоного кольору з RGB
                                                                // між квадратами, між двома сусідніми квадратами
        int dg = (gradient.getGreen() - gradient2.getGreen() )/n;//визначення різниці зеленого кольору з RGB
                                                                 // між квадратами, між двома сусідніми квадратами
        int db = (gradient.getBlue() - gradient2.getBlue() )/n;//визначення різниці синього кольору з RGB
                                                                // між квадратами, між двома сусідніми квадратами
        int width = getWidth()-n/4-30; // визначення ширини і висоти квадрату

        int lefty = (int) (getHeight()/1.75-width/(2*n)); //визначення значення координати y початку
        int left = 12; //початок по координаті x
        for (int i = 0; i<n; i++) {     //ініціалізація квадратів у масиві
            this.square[i] = new Square(left,lefty,width/n,r,g,b,numbers[i]);
            this.square[i].setDigit(digit);
            left = (left + width /n + 1);
            r+=dr;
            g+=dg;
            b+=db;
        }
        check = true; // встановлюємо флажок check на true чим включаємо перемальовуваати панель
    }

    public void paint (Graphics g) {
        int n = this.n;
        Graphics2D g2d = (Graphics2D) g;
        super.paint(g2d);//виклик конструктору базового класу
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//всановлюємо спеціальні ключі для правильного
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);// рендорингу зображення нашій графічній компоненті
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        //Хоча в нас і є перевірка чи проініціалізовані змінні, але всерівно при роботі
        // з графікою можуть появитися помилки тому краще помістити в функції g2d компонети
        // у блок try-catch
        try {
            for (int i = 0; i<=n-1; i++) {//перемальовуємо масив наших увадратів
                this.square[i].draw(g2d);
            }
            g2d.drawImage (titleimg, (int) (getWidth()*0.35), (int) (getHeight()*0.01),this);//малюємо титульне зображення
            for (int i = 0; i<2; i++) {//перемальовуємо рамки, які шукають найменше значення
                int x1 = this.framerect[i][0];
                int y1 = this.framerect[i][1];
                int r1 = this.framerect[i][2];
                int g1 = this.framerect[i][3];
                int b1 = this.framerect[i][4];
                int w1 = this.framerect[i][5];
                final int thickness = 3;//товщина рамки
                Stroke oldStroke = g2d.getStroke();
                g2d.setStroke(new BasicStroke(thickness));//встаовлюэмо товщину компоненти g2d рівній 3
                g2d.setColor(new Color(r1, g1, b1));//встановлюэмо колір рамки
                g2d.drawRect(x1, y1, w1, w1);//малюємо рамку
                g2d.setStroke(oldStroke);//повертаємо товщину компоненти g2d
            }
        }catch (Exception ignored){//щоб не обробляти помилку називаємо її ignored
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint(); // перемальовуємо панель
    }
}
