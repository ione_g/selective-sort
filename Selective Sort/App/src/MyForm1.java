

import util.Sleep;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Objects;


public class MyForm1 extends JFrame implements ActionListener {
    int theme = 0;
    private final MyPanel sortArray;
    private JButton button_1;
    private JButton button_2;
    private JButton button_3;
    private JButton button_4;
    private JButton button_5;
    private JTextField textField_1;
    private Label label_1;
    Color backgroundcolor = Color.darkGray;
    private JMenuItem themeItem;
    private JMenuItem setBackground;
    private JMenuItem setFrameRect;

    private JMenuItem author;
    private JMenuItem aboutproduct;
    private JMenuItem minimize;

    private Scrollbar speedbar;
    MyForm1(){
        this.setTitle("Selective sort visualization");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(backgroundcolor);
        this.sortArray = new MyPanel();
        initialMenuBar();
        this.add(sortArray);
        this.pack();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
        this.sortArray.setLayout(null);

        initialFormComponents();


    }
    private void initialFormComponents() {
        int y = (int) (sortArray.getHeight()*0.66);
        textField_1 = new JTextField("15");

        //Button 1 Start sort
        button_1 = new JButton();
        button_1.setText("Start sort");
        button_1.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));
        button_1.setBackground(null);
        button_1.setForeground(Color.white);
        button_1.setFocusable(false);
        button_1.setBorder(BorderFactory.createLineBorder(Color.white));
        button_1.setBounds(100,y,300,60);
        button_1.setVisible(true);
        button_1.addActionListener(this);
        //button 5 Immediately sort
        button_5 = new JButton();
        button_5.setText("Immediately sort");
        button_5.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));
        button_5.setBackground(null);
        button_5.setForeground(Color.white);
        button_5.setFocusable(false);
        button_5.setBorder(BorderFactory.createLineBorder(Color.white));
        button_5.setBounds(100,y,300,60);
        button_5.setVisible(false);
        button_5.addActionListener(e -> sortArray.speed = 1);
        //Button 2 Shuffle
        button_2 = new JButton("Shuffle array");
        button_2.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));
        button_2.setBackground(null);
        button_2.setForeground(Color.white);
        button_2.setFocusable(false);
        button_2.setBorder(BorderFactory.createLineBorder(Color.white));
        button_2.setBounds(450,y,300,60);
        button_2.setVisible(true);
        button_2.addActionListener(this) ;

        //Button 3 change numbers of squares
        button_3 = new JButton("Set number of squares");
        button_3.setBounds(800,y,300,60);
        button_3.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));
        button_3.setBackground(null);
        button_3.setForeground(Color.white);
        button_3.setFocusable(false);
        button_3.setBorder(BorderFactory.createLineBorder(Color.white));
        button_3.addActionListener(this) ;

        //Button 4
        button_4 = new JButton("Set with another gradient");
        button_4.setBounds(1250,y,200,60);
        button_4.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));
        button_4.setBackground(null);
        button_4.setForeground(Color.white);
        button_4.setFocusable(false);
        button_4.setBorder(BorderFactory.createLineBorder(Color.white));
        button_4.addActionListener(this) ;

        //text Field input number of squares
        textField_1.setBounds(1150,y,60,60);
        textField_1.setCaretColor(Color.white);
        textField_1.setHorizontalAlignment(JTextField.CENTER);
        textField_1.setBackground(null);
        textField_1.setForeground(Color.white);
        textField_1.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 20));

        label_1 = new Label("Change speed visualization");
        label_1.setBackground(null);
        label_1.setIgnoreRepaint(false);
        label_1.setForeground(Color.white);
        label_1.setBounds((int)(getWidth()*0.435), (int) (getHeight()*0.74),250,50);
        label_1.setFont(new Font(Font.SANS_SERIF,  Font.BOLD, 16));


        Dictionary<Integer, JLabel> labels = new Hashtable<>();
        labels.put(-400, new JLabel("<html><font color=white size=4>slow"));
        labels.put(-250, new JLabel("<html><font color=white size=4>medium"));
        labels.put(-100, new JLabel("<html><font color=white size=4>fast"));

        JSlider speedslider = new JSlider(JSlider.HORIZONTAL,-400,-100, -150);
        speedslider.setBounds((int) (getWidth()*0.3),(int)(getHeight()*0.8), (int) (this.getWidth()*0.4),100);
        speedslider.setBackground(null);
        speedslider.setLabelTable(labels);
        speedslider.setPaintLabels(true);
        speedslider.setMinorTickSpacing(50);
        speedslider.setMajorTickSpacing(150);


        speedslider.setPaintTicks(true);


        speedslider.addChangeListener(e -> {
            Runnable speedRunnable = () -> sortArray.speed = Math.abs( speedslider.getValue());
            Thread speedThreed = new Thread(speedRunnable);
            speedThreed.start();
        });

        //Scrollbar to set speed
        speedbar = new Scrollbar(Scrollbar.HORIZONTAL,-150,5,-400,-100);
        speedbar.setBlockIncrement(20);
        speedbar.setBackground(Color.white);
        speedbar.setBounds((int) (getWidth()*0.3),(int)(getHeight()*0.8), (int) (this.getWidth()*0.4),25);
        speedbar.addAdjustmentListener(e -> {
            Runnable speedRunnable = () -> sortArray.speed = Math.abs( speedbar.getValue());
            Thread speedThreed = new Thread(speedRunnable);
            speedThreed.start();

        });

        sortArray.add(button_1);
        sortArray.add(button_2);
        sortArray.add(button_3);
        sortArray.add(button_4);
        sortArray.add(button_5);
        sortArray.add(textField_1);
        sortArray.add(speedslider);
        sortArray.add(label_1);
    }
    private void initialMenuBar () {
        ImageIcon themeicon = new ImageIcon(Objects.requireNonNull(MyForm1.class.getResource("numbers/theme.png")));
        ImageIcon backgroundicon = new ImageIcon(Objects.requireNonNull(MyForm1.class.getResource("numbers/backgroundbrush.png")));
        ImageIcon frameicon = new ImageIcon(Objects.requireNonNull(MyForm1.class.getResource("numbers/paintbrush.png")));

        Image newimg = themeicon.getImage().getScaledInstance(20, 20,  Image.SCALE_AREA_AVERAGING); // scale it the smooth way
        themeicon = new ImageIcon(newimg);  // transform it back
        newimg = frameicon.getImage().getScaledInstance(20, 20,  Image.SCALE_AREA_AVERAGING); // scale it the smooth way
        frameicon = new ImageIcon(newimg);  // transform it back
        newimg = backgroundicon.getImage().getScaledInstance(20, 20,  Image.SCALE_AREA_AVERAGING); // scale it the smooth way
        backgroundicon = new ImageIcon(newimg);  // transform it back

        JMenuBar menubar = new JMenuBar();
        menubar.setVisible(true);

        JMenu view = new JMenu("View");
        themeItem = new JMenuItem("Theme",themeicon);
        setBackground = new JMenuItem("Background color",backgroundicon);
        setFrameRect = new JMenuItem("Rectangle frame color",frameicon);


        setBackground.addActionListener(this);
        setFrameRect.addActionListener(this);
        themeItem.addActionListener(this);

        JMenu about = new JMenu("Help");
        aboutproduct = new JMenuItem("About");
        author = new JMenuItem("Author");

        aboutproduct.addActionListener(this);
        author.addActionListener(this);

        JMenu program = new JMenu("Program");
        JMenuItem close = new JMenuItem("Close");
        minimize = new JMenuItem("Minimize");
        close.addActionListener(e -> System.exit(0));
        minimize.addActionListener(this);

        menubar.add(program);
        program.add(close);
        program.add(minimize);
        menubar.add(view);
        view.add(themeItem);
        view.add(setBackground);
        view.add(setFrameRect);
        menubar.add(about);
        about.add(aboutproduct);
        about.add(author);
        this.setJMenuBar(menubar);
    }
    private void shuffleAndWait() {
        this.sortArray.shuffleColor();
        Sleep.sleepFor(Sleep.secondsToNano(2L));
    }
    public void initial() {
        this.sortArray.n = 15;
        this.sortArray.refresher();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == setBackground) {
            Color color = JColorChooser.showDialog(null,"Pick a color", Color.darkGray);
            sortArray.setBackground(color);
            label_1.setBackground(color);
        }
        if (e.getSource() == setFrameRect) {
            Color localcr = JColorChooser.showDialog(null, "Pick a color", Color.white);
            for (int i = 0; i < sortArray.n; i++) {
                sortArray.square[i].setFramecolor(localcr);
            }
        }
        if (e.getSource() == minimize) {
            this.setExtendedState(Frame.ICONIFIED);
        }
        if (e.getSource() == themeItem) {
            theme += 1;
            if (theme == 2) theme = 0;
            for (int i = 0; i < sortArray.n; i++) {
                sortArray.square[i].setTheme(theme);
            }
        }
        if (e.getSource() == author){
            JOptionPane.showMessageDialog(null, "Created by Ivan Hrymaliuk \nstudent Fei-22","Authored",JOptionPane.INFORMATION_MESSAGE);
        }
        if (e.getSource() == aboutproduct){
            JOptionPane.showMessageDialog(null, """
                    Selective Sort Visualizier
                    \s
                    Алгоритм працює таким чином:
                    Знаходить у списку найменше значення
                    Міняє його місцями із першим значеннями у списку
                    Повторює два попередніх кроки, доки список не завершиться (починаючи з наступної позиції)
                    Фактично, таким чином ми поділили список на дві частини: перша (ліва) — повністю відсортована, а друга (права) — ні.""","About",JOptionPane.QUESTION_MESSAGE);

        }
        if (e.getSource() == button_1) {
            button_1.setEnabled(false);//забороняємо клацати по кнопках, які можуть привести до
            button_1.setVisible(false);//не бажаних результатів і як наслідок поломки програми
            button_2.setEnabled(false);
            button_3.setEnabled(false);
            button_4.setEnabled(false);
            button_5.setVisible(true);//кнопка, яка миттєво посортує квадратів стає видимою
            Runnable r = () -> {
                sortArray.speed = Math.abs( speedbar.getValue());
                sortArray.runSort();
                button_1.setEnabled(true);//клацання після закінчення сортування можна знову дозволити
                button_1.setVisible(true);
                button_2.setEnabled(true);
                button_3.setEnabled(true);
                button_4.setEnabled(true);
                button_5.setVisible(false);//кнопка, яка миттєво сортує квадрати зникає
            };
            Thread  t = new Thread(r);
            t.start();
        }
        if (e.getSource() == button_2) {//перемішування масиву квадратів
            Runnable r = this::shuffleAndWait;
            Thread t1 = new Thread(r);
            t1.start();
        }
        if (e.getSource() == button_3) {//зміна кількості квадратів і(або) їх зачень number
            Runnable r = () -> {
                sortArray.check = false;
                try {
                    sortArray.n = Integer.parseInt(textField_1.getText());
                    if (Math.abs(((double)sortArray.gradient.getBlue()-sortArray.gradient2.getBlue())/sortArray.n) < 0.5||
                            (Math.abs((double)sortArray.gradient.getGreen()-sortArray.gradient2.getGreen())/sortArray.n) < 0.5||
                            (Math.abs((double)sortArray.gradient.getRed()-sortArray.gradient2.getRed())/sortArray.n) < 0.5)
                        throw new Exception("Неправильний градієнт або завелика кількість квадратів");
                    sortArray.refresher();
                    if (sortArray.n > 40) theme = 0;
                    for (int i = 0; i < sortArray.n; i++) {
                        sortArray.square[i].setTheme(theme);
                    }

                } catch (IllegalArgumentException e1) {
                    JOptionPane.showMessageDialog(new JOptionPane(), "Enter a positive integer number");
                    textField_1.setText("");
                    textField_1.setFocusable(true);
                } catch (Exception e1){
                    JOptionPane.showMessageDialog(new JOptionPane(), e1.getMessage());
                    sortArray.n = 15;
                    sortArray.refresher();
                }
            };
            Thread t1 = new Thread(r);
            t1.start();
        }
        if (e.getSource() == button_4){

            try {
                theme = 0;
            for (int i = 0; i < sortArray.n; i++) {
                sortArray.square[i].setTheme(theme);
            }
            Color gradient = JColorChooser.showDialog(null, "Pick a color", Color.white);
            Color gradient2 = JColorChooser.showDialog(null, "Pick a color", Color.white);
            if (Math.abs(((double)sortArray.gradient.getBlue()-sortArray.gradient2.getBlue())/sortArray.n) < 0.5||
                    (Math.abs((double)sortArray.gradient.getGreen()-sortArray.gradient2.getGreen())/sortArray.n) < 0.5||
                    (Math.abs((double)sortArray.gradient.getRed()-sortArray.gradient2.getRed())/sortArray.n) < 0.5)
                throw new Exception("Шось не так з градієнтом");
                if (gradient.equals(gradient2)){
                JOptionPane.showMessageDialog(null,"Ви вибрали однакові кольори\n" +
                                                                        "Сортування можливе тільки по цифрах");

                theme = 1;
                button_3.doClick();
            }else{
                sortArray.gradient2 = gradient;
                sortArray.gradient = gradient2;
            }}catch (Exception e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage());
            }

                button_3.doClick();
        }

    }


}
