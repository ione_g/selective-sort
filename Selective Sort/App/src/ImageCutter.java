

import java.awt.image.BufferedImage;

public class ImageCutter {
    private final BufferedImage image;

    public ImageCutter(BufferedImage image) {
        this.image =  image;
    }

    public BufferedImage grabImage(int col,int row,int width,int height) {
        return image.getSubimage((col * width)-width,(row*height)-height,width,height);
    }
}
